
int sensorvalue = 0;

int dArray [4] = {D0, D2, D5, D7};

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(D0, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D7, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  
  sensorvalue = analogRead(A0)/256; // 0-1023

  for(int i = 0; i < 4; i++){
    digitalWrite(dArray[i], i > sensorvalue);
  }

  delay(10);
  
}


////////////////////////////////


int sensorvalue = 0;

int dArray [5] = {D0, D2, D5, D6, D7, };

void setup() {
  pinMode(D0, OUTPUT);
  pinMode(D1, INPUT);
  pinMode(D2, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(D6, OUTPUT);
}

void loop() {

  digitalRead(D1); //mode lecture
     
}

////////////////////////////////////

int dArray [4] = {D1, D3, D5, D7};
int i = 0;
void setup()
{
    Serial.begin(115200);
    pinMode(D0, INPUT);
    pinMode(D1, OUTPUT);

    pinMode(D2, INPUT);
    pinMode(D3, OUTPUT);

    pinMode(D4, INPUT);
    pinMode(D5, OUTPUT);

    pinMode(D6, INPUT);
    pinMode(D7, OUTPUT);
}
void loop()
{



    int etatBoutonD0 = digitalRead(D0);
    int etatBoutonD2 = digitalRead(D2);
    int etatBoutonD4 = digitalRead(D4);
    int etatBoutonD6 = digitalRead(D6);

    int ebArray [4] = {etatBoutonD0, etatBoutonD2, etatBoutonD4, etatBoutonD6};

    for(int i = 0; i < 4; i++)
    {
        if (ebArray[i] == 1) {
            digitalRead(D1) == HIGH ? digitalWrite(dArray[i], LOW) : digitalWrite(dArray[i], HIGH);
            delay(100);
        }
    }

    delay(5);

}
///////////////////////////////////

int dArray [4] = {D1, D3, D5, D7};
int ebArray [4] = {D0, D2, D4, D6};

int i = 0;
void setup()
{
    Serial.begin(115200);
    pinMode(D0, INPUT);
    pinMode(D1, OUTPUT);

    pinMode(D2, INPUT);
    pinMode(D3, OUTPUT);

    pinMode(D4, INPUT);
    pinMode(D5, OUTPUT);

    pinMode(D6, INPUT);
    pinMode(D7, OUTPUT);
}
void loop()
{
    bool pauseNeeded = false;


    for(int i = 0; i < 4; i++)
    {
        if (digitalRead(ebArray[i]) == 1) {
            digitalWrite(dArray[i], !digitalRead(dArray[i]));
            pauseNeeded = true;
        } 
    }

    if(pauseNeeded){
        delay(300);
    }

    delay(5);

}

////////////////////////////////

#include <iostream>
#include <vector>

int dArray [4] = {D1, D3, D5, D7};
int ebArray [4] = {D0, D2, D4, D6};


void setup()
{
    Serial.begin(115200);
    pinMode(D0, INPUT); // BOUTON 1
    pinMode(D1, OUTPUT); // LED ROUGE

    pinMode(D2, INPUT); // BOUTON 2
    pinMode(D3, OUTPUT); // LED VERT

    pinMode(D4, INPUT); // BOUTON 3
    pinMode(D5, OUTPUT); // LED JAUNE

    pinMode(D6, INPUT); // BOUTON 4
    pinMode(D7, OUTPUT); // LED BLUE
}
void loop()
{   
    Serial.println("Phase d'enregistrement");
    std::vector<int> sequence; // initialisation liste
    for (int n = 0; n < 4; n++) { // vérif BOUTON n
        while (sequence.size() < 4) { //
            if (digitalRead(ebArray[n]) == 1) { // trie des BOUTONS
                sequence.push_back(ebArray[n]); // stockage dans la liste
            }
        } 
    }
    Serial.println("Phase de reproduction");
    for (int x = 0; x < 4; x++) {
        digitalWrite(sequence[x], LOW);
    }

    delay(5);

}


void loopLeds(){

    for (int i = 0; i < 4; i++) {
        for (int x = 0; x < 4; x++) {
            if(x == i){
                digitalWrite(ledArray[x], HIGH);
            } else {
                digitalWrite(ledArray[x], LOW);
            }
        }

        delay(100);
    }
}
void inverseLoopLeds(){

    for (int i = 3; i >= 0; i--) {
        for (int x = 0; x < 4; x++) {
            if(x == i){
                digitalWrite(ledArray[x], HIGH);
            } else {
                digitalWrite(ledArray[x], LOW);
            }
        }

        delay(100);
    }
}
void randomiseLeds(){

    for (int x = 0; x < 4; x++) {
        long rand = random(0, 2);
        if(rand){
            digitalWrite(ledArray[x], HIGH);
        } else {
            digitalWrite(ledArray[x], LOW);
        }
    }

    delay(200);
}


void binaryLeds(){
    

    for (int i = 0; i < 16; i++) {
        for (int x = 0; x < 4; x++) {
            if((int(pow(2, x)) & i) > 0){
                digitalWrite(ledArray[x], HIGH);
            } else {
                digitalWrite(ledArray[x], LOW);
            }
        }

        delay(100);
    }
}

/////////////////////////////////////////////////

#define SHCP 16
#define STCP 4
#define DS 15

float sensorvalue = 0;
boolean reg[] = {false, false, false, false, false, false, false, false};
int cursor = 0;

boolean lastValue = false;

void setup()
{
    Serial.begin(115200);
    pinMode(SHCP, OUTPUT);
    pinMode(STCP, OUTPUT);
    pinMode(DS, OUTPUT);
}

void moveRegister(int places){
    for(int i=0; i<places; i++){
        // Je passe le pin SHCP a zero en me preparant pour un decallage        
        digitalWrite(SHCP, LOW);

        // Je repasse a un pour me decaler d'une collone et je recomence mon for tant que i < 8 pour me decaler 8 fois et ecrire sur tout les GPIO
        digitalWrite(SHCP, HIGH);
    }
}

void applyChanges(){

    //Broche pour appliquer les changements STCP: on le passe a 0 pour appliquer les changement plus tards en le passant a 1. Aucun changement peut avoir lieu avec STCP a 1
    digitalWrite(STCP, LOW);  
    //Je passe STCP  a 1 pour appliquer les changements 
    digitalWrite(STCP, HIGH);
}

void writeRegister(){ 

    sensorvalue = analogRead(A0)/100.0;
    
    for(int i=0; i<8; i++){
        Serial.print(i);
        Serial.print("\t");
        Serial.println(sensorvalue);


        if (sensorvalue < 1){
            digitalWrite(DS, true);
        } else if(sensorvalue > 7.9 && i == 7) {
            lastValue = !lastValue;
            digitalWrite(DS, lastValue);
        } else {
            digitalWrite(DS, i >= (sensorvalue - 1));

        }
        moveRegister(1);
        // Je lit mon tableau "reg" pour i allant de 0 a 8 voir ci dessus. J'applique l'état boolean inverse vut que mes leds s'allume en état basse. 
        //  digitalWrite(DS, i > sensorvalue); 
   

    }
    applyChanges();
}

void clearRegister(){
    for(int i=0; i<8; i++){
        reg[i] = false;
        digitalWrite(DS, !reg[i]); 
        moveRegister(1);
    }
    applyChanges();
}

void loop(){
    writeRegister();
    delay(100);
}

/////////////////////////////////////////

#define SRCLK 16
#define RCLK 4
#define SER 15

float sensorvalue = 0;
boolean reg[] = {false, false, false, false, false, false, false, false};
int cursor = 0;

boolean lastValue = false;

void setup()
{
    Serial.begin(115200);
    pinMode(SRCLK, OUTPUT);
    pinMode(RCLK, OUTPUT);
    pinMode(SER, OUTPUT);
}

void moveRegister(int places){
    for(int i=0; i<places; i++){
        // Je passe le pin SRCLK a zero en me preparant pour un decallage        
        digitalWrite(SRCLK, LOW);

        // Je repasse a un pour me decaler d'une collone et je recomence mon for tant que i < places pour me decaler autant de fois que demander
        digitalWrite(SRCLK, HIGH);
    }
}

void applyChanges(){

    //Broche pour appliquer les changements RCLK: on le passe a 0 pour  s'assure d'obtenir un front montant en le passant a 1.
    digitalWrite(RCLK, LOW);  
    //Je passe donc RCLK  a 1 pour appliquer les changements 
    digitalWrite(RCLK, HIGH);
}

boolean digiw(boolean boul){
    //je choisis quelle valeur je vais garder pour la colonnes grace a SER
    digitalWrite(SER, boul);

    //Decalage pour mettre SER sur la collone actuel
    // POur ce faire je massure que SRCLK est a zero car je dois le passer a 1 pour me decaler d'une collone   
    digitalWrite(SRCLK, LOW);
    // Je le passe a 1
    digitalWrite(SRCLK, HIGH);

    // Je retour boul pour simplifier la syntaxe si jamais on veut conserver l'etat definie (ex: reg[1] = digiw(true);)
    return boul;
}

void writeRegister(){ 
    
    for(int i=0; i<8; i++){
        for(int x=0; x<8; x++){
            if (x == i) {
                digiw(true);
            }else{
                digiw(false);
            }
        }
        applyChanges();
        delay(1000);
    }
}

void writeRegister2(){ 
    
    for(int i=7; i>=0; i--){
        for(int x=0; x<8; x++){
            if (x == i) {
                digitalWrite(SER, reg[x] = true);
            }else{
                digitalWrite(SER, reg[x] = false);
            }
            moveRegister(1);
        }
        
        applyChanges();
        delay(200);
    }
}

void loop(){
    writeRegister();
    writeRegister2();
}
////////////////////////////////
//int sensorvalue = 0;


void setup(){

    Serial.begin(115200);
    pinMode(D0, OUTPUT);
    pinMode(D2, OUTPUT);
    pinMode(D3, OUTPUT);

    pinMode(A0, INPUT);
}

void pwm(){
    
}

void loop(){

    // sensorvalue = analogRead(A0)*1.05;
    // Serial.print(sensorvalue/100),Serial.print((sensorvalue%100)/10),Serial.println(sensorvalue%10);

    colorForTime(0, 0, 0, 1000);
    delay(16);
}

void color(int r, int g, int b){
    digitalWrite(D0, HIGH); // RED
    digitalWrite(D2, HIGH); // GREEN
    digitalWrite(D3, HIGH); //BLUE

    for(int i = 0; i < 10; i++){
        if(i >= r){
            digitalWrite(D0, LOW); // RED
        }
        if(i >= g){
            digitalWrite(D2, LOW); // RED
        }
        if(i >= b){
            digitalWrite(D3, LOW); // RED
        }
        delay(1000);
    }

}


void colorForTime(int r, int g, int b, int p_delay){

    for(int i = 0; i < p_delay/10; i++){
        int r = random(0, 10);
        int g = random(0, 10);
        int b = random(0, 10);
        color(r, g, b);
    }
}

