#include <ESP8266WiFi.h>

WiFiServer wifiServer(80);

void setup()
{
    Serial.begin(115200);
    Serial.println();

    pinMode(D0, OUTPUT);
    pinMode(D1, OUTPUT);
    pinMode(D2, OUTPUT);

    digitalWrite(D0, HIGH);
    digitalWrite(D1, HIGH);
    digitalWrite(D2, HIGH);

    WiFi.begin("Redmi", "");

    Serial.print("Connecter");

    while(WiFi.status() != WL_CONNECTED){
        delay(300);
        Serial.print(".");
        digitalWrite(D0, HIGH);
        delay(300);
        digitalWrite(D0, LOW);
    }
    Serial.println();

    Serial.print("Connecter, IP :");
    Serial.println(WiFi.localIP());
    digitalWrite(D0, LOW);

    wifiServer.begin();
}

void loop()
{
  WiFiClient client = wifiServer.available();
 
  if (client) {
    Serial.println("Client connected");
    while (client.connected()) {
 
      while (client.available()>0) {
          client.write(".");
          Serial.write(".");
          //delay(100);
      }
      delay(10);
    }
    client.stop();
    Serial.println("Client disconnected");
  }
}