int sensorvalue = 0;

int dArray [4] = {D0, D2, D5, D7};

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(D0, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D7, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  
  sensorvalue = analogRead(A0)/256; // 0-1023

  for(int i = 0; i < 4; i++){
    digitalWrite(dArray[i], i > sensorvalue);
  }

  delay(10);
  
}